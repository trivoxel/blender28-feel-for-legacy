# Blender 2.8 Feel for Legacy

Blender 2.7X keymap and themes I designed to apply the look and feel of Blender 2.80 to legacy versions of Blender.

## Video demonstration of it working:
[![Video demo of theme and keymap](https://img.youtube.com/vi/o_E4kDH3e3A/0.jpg)](https://www.youtube.com/embed/o_E4kDH3e3A)